
<!-- README.md is generated from README.Rmd. Please edit that file -->

![](man/figures/logo_phylostems.png)

<!-- badges: start -->

[![Lifecycle:
maturing](https://img.shields.io/badge/lifecycle-maturing-blue.svg)](https://www.tidyverse.org/lifecycle/#maturing)
<!-- badges: end -->

**phylostems** is a graphical tool developped by CIRAD (UMR PVBMT) to
explore temporal signal at various evolutionary scales in heterochronous
nucleic acid sequences datasets.

-----

  - Current version: 0.1.1
  - Contact email: <phylostems@gmail.com>  
  - Developped at CIRAD by Anna Doizy, Amaury Prin, Frederic Chiroleu,
    Guillaume Cornu and Adrien Rieux  
  - Associated publication: coming soon

Two example trees (Hantaviruses & *X.fastidiosa*) taken as illustration
in the **phylostems** publication stands in the [`dataset`
folder](https://gitlab.com/cirad-apps/phylostems/-/tree/master/datasets).

## Online usage

Just go to <https://pvbmt-apps.cirad.fr/apps/phylostems/>.

**Note**: The online version of **phylostems** allows uploading trees
with 1500 sequences/tips at maximum.

## Local usage

You can install **phylostems** R package from
[gitlab](https://gitlab.com/cirad-apps/phylostems) with:

``` r
library(remotes)
install_gitlab("cirad-apps/phylostems@0.1.1") # last release
# or
install_gitlab("cirad-apps/phylostems") # development version
```

Run this command to launch the app locally:

``` r
library(phylostems)
run_app()
```

**Note**: When executed locally, it is possible to analyse trees with
more than 1500 sequences. To do so, you can set another value to the
`max_tree_size` option:

``` r
options(phylostems.max_tree_size = 3000)
```
