#' Define border colour of the tree nodes
#'
#' @format named character
"node_colours"

#' Define filling colour of the tree nodes
#'
#' @format named character
"node_filling"

#' Define shape of the tree nodes
#'
#' @format named character
"node_shapes"

#' Define size of the tree nodes
#'
#' @format named character
"node_sizes"

